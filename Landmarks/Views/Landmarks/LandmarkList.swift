//
//  LandmarkList.swift
//  Landmarks
//
//  Created by Brody Gao on 2023/2/26.
//

import SwiftUI

struct LandmarkList: View {
    @EnvironmentObject var modelData:ModelData
    @State private var showFavoriteOnly = false
    var filterdlandmarks:[Landmark]{
        modelData.landmarks.filter{
            landmark in (!showFavoriteOnly||landmark.isFavorite)
        }
        
    }
    var body: some View {
        NavigationView{
            List{
//                Toggle(isOn: $showFavoriteOnly) {
//                    Text("Favorite Only")
//                }
                Toggle("Favorite Only", isOn: $showFavoriteOnly)
                ForEach(filterdlandmarks){
                    landmark in
                    NavigationLink(destination: LandmarkDetail(landmark: landmark), label: {LandmarkRow(landmark:landmark)})
                }
            }.navigationTitle("Landmarks")
                
        }
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone 13","iPhone 14"], id: \.self) { deviceName in
            LandmarkList().environmentObject(ModelData())
                        .previewDevice(PreviewDevice(rawValue: deviceName))
                        .previewDisplayName(deviceName)
                }
    }
}
