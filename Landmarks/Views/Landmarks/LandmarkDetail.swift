//
//  LandmarkDetail.swift
//  Landmarks
//
//  Created by Brody Gao on 2023/3/16.
//

import SwiftUI

struct LandmarkDetail: View {
    @EnvironmentObject var modeData:ModelData
    var landmark:Landmark
    
    var landmarkIndex:Int{
        modeData.landmarks.firstIndex(where:{
            $0.id == landmark.id
        })!
    }
    var body: some View {
        ScrollView {
            MapView(coordinate: landmark.locationCoordinate).ignoresSafeArea(edges: .top)
                .frame(height: 300)
            
            CircleImage(image:landmark.image).offset(y:-130).padding(.bottom,-130)
            
            VStack(alignment: .leading) {
                HStack {
                    Text(landmark.name)
                        .font(.title)
                    FavoriteButton(isSet: $modeData.landmarks[landmarkIndex].isFavorite)
                }
                HStack {
                    Text(landmark.park)
                        .font(.subheadline)
                    Spacer()
                    Text(landmark.state).font(.subheadline)
                }.font(.subheadline).foregroundColor(.secondary)
                
                Divider()
                
                Text("About \(landmark.name)").font(.title2)
                Text(landmark.description).frame(alignment: .leading)
                
            }.padding()
            
        }.navigationTitle(landmark.name)
            .navigationBarTitleDisplayMode(.inline)
    }
}

struct LandmarkDetail_Previews: PreviewProvider {
    static var modeData = ModelData()
    static var previews: some View {
        LandmarkDetail(landmark:modeData.landmarks[0]).environmentObject(modeData)
    }
}
