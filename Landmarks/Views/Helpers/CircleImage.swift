//
//  CircleImage.swift
//  Landmarks
//
//  Created by Brody Gao on 2023/2/24.
//

import SwiftUI

struct CircleImage: View {
    var image:Image
    
    var body: some View {
        image
            .frame(width: 260.0, height: 260.0)
            .clipShape(Circle()).overlay(Circle().stroke(.white,lineWidth: 4))
            .shadow(radius: 7)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage(image: Image("summer"))
    }
}
