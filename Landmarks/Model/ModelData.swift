//
//  ModelData.swift
//  Landmarks
//
//  Created by Brody Gao on 2023/2/25.
//

import Foundation
import Combine

final class ModelData:ObservableObject{
    @Published var landmarks:[Landmark] = load("landmarkData.json")
}



func load<T:Decodable>(_ filename:String) -> T{
    let data:Data
    
    guard  let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else{
        fatalError("Could't find \(filename) in main boundle")
    }
    
    do{
        data = try Data(contentsOf: file)
    }catch{
        fatalError("Could't load \(filename) from main boundle \n\(error)")
    }
    
    do{
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }catch{
        fatalError("Could't pase \(filename) as \(T.self):\n\(error)")
    }
}
