//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Brody Gao on 2023/2/14.
//

import SwiftUI

@main
struct LandmarksApp: App {
    @StateObject private var modeData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(modeData)
        }
    }
}
